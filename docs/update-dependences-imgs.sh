#!/bin/bash





echo "Creating image for senia-cdd-desktop"
debtree senia-cdd-desktop --max-depth=2 | dot -T png -o senia-cdd-desktop.png

echo "Creating image for senia-dependences"
debtree senia-dependences --max-depth=1 | dot -T png -o senia-dependences.png
