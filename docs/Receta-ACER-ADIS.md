# Documentación ACER - ADIS 2023

Para los diferentes ACER que se han suministrado con las ADIs, se ha preparado un paquete 
que configura algunos parámetros.

## ActualizaciónJunio 2024

- Entrar en LliureX en el Grub. 
- Seleccionar otro login (Defecto) y entrar con tic.
- Abrir una terminal y ejecutar las siguientes ordenes:

```bash
sudo lliurex-upgrade -u
## Mientras se actualiza, podeis ir rellenando la Hoja
## de calculo.
sudo reboot
```

Esto hay que hacerlo en todos los Acer de las ADIs, así prepararé una excel y os vais apuntando los hechos.


## Windows 11

Si algún portátil arranca con Windows por defecto, debéis editar el GRUB para cambiar el arranque por defecto. 

### Pasos

1. Entrar en LliureX.
2. Iniciar sesión con `tic`
3. `sudo nano /etc/default/grub`
4. Dejáis la variable `GRUB_DEFAULT=0`.
5. `sudo update-grub` 
6. `sudo reboot`

Probar que funciona. Si no arranca, obtened la `ip` usando el comando `ip a` y buscadme. 

Recordad que si no funciona el entorno gráfico, podéis iniciar sesión en una Wifi en modo Consola con el comando `nmtui`.

Si algún portátil no tiene arranque con GRUB, cogedlo y subidlo al departamento y ponemos un tiquet para que se arregle de manera remota.



