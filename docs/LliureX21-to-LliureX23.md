# Guia de Actualización de LliureX 21 a 23


## Motivación

Vamos a pasar de LliureX 21, basado en Focal Fossa a Lliurex 23, basado en Jammy. Además de ganar todas 
las actualizaciónes provenientes de Ubuntu, contaremos con la últimísima versión de las maravillosas aplicaciones
que se han realizado para La Senia durante estos últimos 2 años.


## Windows y LliureX

Dentro de la decisión de Consellería de entregar equipos duales, creo que es conveniente en un proceso de estas 
características, donde el reinicio está a la orden del día, es conveniente establecer en el **GRUB** el sistema
operativo por *DEFECTO* que vamos a estar actualizando. En este caso LliureX: 

```shell

# ... Sigue mas arriba

GRUB_DEFAULT=0
GRUB_TIMEOUT_STYLE=hidden
GRUB_TIMEOUT=0

# ... Sigue mas abajo 

```

Una vez cambiado, pues :


```shell
sudo update-grub
```

y ya tendremos el **GRUB** preparado y listo.

## Guia técnica

Lo primero es **actualizar a tope** dentro de la propia distribución:


```shell

sudo lliurex-upgrade -u

```


Una vez actualizado, reiniciamos y nos aseguramos de que el**GRUB** arranca por defecto en LliureX, para quitarnos
de problemas.
