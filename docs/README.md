# Senia over LliureX 21 (focal)

Javier Daroqui y Angel han desarrollado un paquete software para aplicar
las configuraciones necesarias para que los portatiles de las ADIs puedan 
trabajar de manera optima en el IES.

Para instalar la configuración necesaria se deben realizar los siguientes pasos 
en los lliurex de las ADIS.

## Añadir el PPA

Añadimos el PPA de la Senia a LliureX.

```shell

sudo add-apt-repository ppa:ticsenia/ppa

```
### Limpiar repositorios

Borramos la mierda del Google Chrome y eso del Geogebra.

```shell
sudo rm /etc/apt/sources.list.d/googl*.list
sudo rm /etc/apt/sources.list.d/geog*.list
```

## Actualizamos los repositorios

```shell
sudo apt update
```

## Instalamos el paquete senia-lliurex-configs

```shell
sudo apt install senia-lliurex-configs
```

## Descripción del paquete

Este paquete instala:

- Una configuración para `netplan` para la Wifi_S de la Senia.
- Una diversion del fichero `Main.qml`
- La configuración del salvapantallas y energia a 1 hora.

Se aceptan mejoras y propuestas, podéis ver el código fuente aqui:

- [Senia LliureX Configs](https://gitlab.com/aberlanas/senia-cdd/-/tree/master/senia-lliurex-configs.install?ref_type=heads)




# Senia over Lliurex 23

Los pasos para dejar un entorno más simpatico para la docencia son:

## Añadir el PPA

Añadimos el PPA de la Senia a LliureX.

```shell

sudo add-apt-repository ppa:ticsenia/ppa

```

## Instalar el mutageno

```shell

sudo apt install senia-lliurex-mutagen

```

## Ejecutar el mutageno

```shell

sudo senia-lliurex-mutagen

```

Este paquete *prepara* LliureX para ser **seniaficado** sin muchos problemas, estad atent@s a la salida
de los comandos, que tal vez se pueda detectar algún error o algo.

Mayormente este script:

- Añade ubuntu al directorio de `sources.list.d/`.
- Fija el pinning a 500.
- Actualiza el repositorio de paquetes.
- Actualiza el `GRUB_DEFAULT`.
- Añade la entrada de Ventoy.
- Instala el metapaquete de senia-cdd-lliurex.
- Instala la basura propietaria de VSCode.

## Reiniciar

Toca reiniciar y ver que todo va bien. Se ha instalado una configuración que pone el tiempo de espera a 1 hora 
en vez de 10 minutos para el salvapantallas.

## Redimensionado

Una vez comprobado que todo está en su sitio, toca arrancar con un Ventoy con la entrada que se ha añadido 
al GRUB y redimensionar el espacio en disco de los sistemas.

Recordad que como está en modo UEFI toca *enrolar* la clave EFI que está dentro de Ventoy. Molaría hacerlo desde 
el Script, pero bueno, son dos segundos en la UEFI de los ordenadores. Se siguen los pasos que se proponen y se selecciona la *key* que está en *Raiz de Ventoy*

PD: Como yo de esto en realidad no sé mucho, si alguien se toma la molestia de explicarlo mejor, se puede incorporar
a la documentación y tal.

Se propone :

- 90 Gigas para Windows.
- 400 Gigas para LinuX.

La intención es dejar Windows sin usar (la mejor decisión *EVER* (después de borrarlo, claro :innocent: ) ).
