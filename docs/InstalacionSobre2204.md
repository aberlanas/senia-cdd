---
title: Guia Instalacion del Metapaquete de la Senia
author: Angel Berlanas Vicente
lang: es-ES
keywords: [senia]
---

# Instalación de la Senia sobre Xubuntu 22.04 - Guia 

## Descripción de los pasos

Leer cada uno de los pasos antes de ejecutarlo.

## Instalación de Xubuntu 22.04

Descargad la imagen ISO de Xubuntu desde la página oficial:

* [ URL de Xubuntu ](https://xubuntu.org/download/)

Una vez descargada la podéis tostar en un usb usando `dd` (por ejemplo).

```shell

usuario@maquina:~$ sudo dd if=/ruta/al/fichero/iso/de/xubuntu.iso of=/dev/miUSBSinParticiones status=progress

```

Cuando acabe, ejecutad `sync` que no está de más.

Durante la instalación, tened en cuenta de debéis instalar con el usuario `tic` y de contraseña podéis poner por
ejemplo:

* `transistor`

No os conectéis a la red para actualizar y no marqueís actualizar la instalación ya que al estar proxificados los
repositorios de Ubuntu en la red de Macrolan nada va a la velocidad que debe desde los repositorios que vienen 
configurados.

Más adelante en el proceso, actualizaremos contra la réplica del servidor interno y todo funcionará como toca.

No instaléis soporte de mp3, ni nada de eso.

## Paso 2

Una vez hayamos reiniciado la máquina, antes de hacer nada más vamos a actualizar los orígenes del software:

El fichero `/etc/apt/sources.list` ha de contener *únicamente* los siguientes repositorios:

```shell
deb http://tic.ieslasenia.org/ubuntu jammy main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-updates main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-security main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-backports main universe restricted multiverse
```

Se realiza un `apt update` y se está atento a posibles fallos, estamos descubriendo varios posibles. Se describen a continuación:

### i386 Not Found

Se trata de máquinas que tienen instalado software de 32 bits y eso ya no está soportado en Jammy, así que mejor deshabilitar esa 
opción desde la base:

```shell
sudo dpkg --remove-architecture i386
```

Si no os deja, habrá que quitar los paquetes que tengamos instalados con esa arquitectura obsoleta:

```shell
LISTA=$(dpkg -l | awk '/^ii/ && $4 == "i386" { print }'  | tr -s " " | cut -d " " -f2 | tr "\n" " " )
sudo apt purge ${LISTA}
```

O, si queréis un *one-liner*:

```shell
LISTA=$(dpkg -l | awk '/^ii/ && $4 == "i386" { print }'  | tr -s " " | cut -d " " -f2 | tr "\n" " " ); sudo apt purge ${LISTA}
```

### icons 64x64 Not found

El paquete appstream es una *castaña* y lo mejor es que lo quitemos para siempre. Si hace falta ya lo añadiremos.

```shell
sudo apt purge appstream

```

Ahora añadiremos el repositorio de la senia en Launchpad

```
sudo add-apt-repository ppa:ticsenia/ppa
```

Y luego de nuevo

```shell
sudo apt update; sudo apt full-upgrade --yes
sudo apt install senia-cdd-xfce
```

Cuando acabe el proceso, ejectuad `senia-version` y comprobad que todo está bien.

A lo largo de la actualización irán surgiendo preguntas de restart de servicios (sí), de instalación de dispositivos de arranque para el GRUB (primer disco presente), de reinicio de `docker`, etc.  No es ciencia de cohetes, seguir vuestra intuición de informáticos y conseguiréis llegar al final.

Una vez actualizado, yo reiniciaría.

```shell
sudo reboot
```

Con esto instalado, ya tendremos el Escritorio configurado, con la *suite* de programas instaladas, utilidades, configuraciones para el alumnado, etc....*IT'S MAGIC!*.

Para asegurarse de todo ha ido bien, se ha desarrollado un software que muestra la versión de la Senia, estará disponible al final de la actualización:

```shell
senia-version
```


