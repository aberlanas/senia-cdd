# Infraestructura del IES La Senia

Este documento trata de describir de una manera bastante exhaustiva y técnica todo lo necesario para el trabajo con Sistemas y Redes que conviven 
en la Infraestructura del IES La Senia.

Se han realizado cambios en el propio documento con el objetivo de *simplificarlo*, e ir añadiendo *poco a poco* la información necesaria para 
describir la Infraestructura y las máquinas que conviven en el IES.

| Actualización del Documento |
|:-------------:|
|20240917     |

# Configuración de la Red del Centro

*Importante*: Todos los equipos salen por la Macrolan.

## Red de Secretaria

Hay una serie de bocas de todos los switches Huawei del centro que se encuentran en la red de secretaria.
En esta red funciona un DHCP no gestionable y que tiene un filtrado diferente del de las aulas. 

No podemos conectar equipos a esta red sin abrir tickets.

## Red del Centro (Aulas)

**Nuestra red de trabajo.**

Esta Red es la definida por la Dirección IP:

*172.29.0.0/23*

Nótese que nos hallamos en una *Doble C* para l@s fans de las categorizaciones
de las Redes en clases, pero que además no importa los dos primeros bits del primer octeto, así que de nuevo,
lo que importa no es cómo las clasificamos sino cómo funcionan ;-) .

Disponemos de 510 direcciones IP válidas en esa red:

172.29.0.1 $\rightarrow$ 172.29.1.254

Sin embargo, para facilitar la administración del centro y ya que no contamos con un DHCP propio ( y ser de esta 
manera más *Consellería-compliant*) lo que está configurado en el Switch del centro (Gestionado por el SAI), es que esta red de 510 direcciones 
está separada en dos mitades, la primera :

- `172.29.0.0/24` $\rightarrow$ NO TIENE DHCP, de esta manera nosotros ponemos IPs fijas en esta red para los equipos de las Aulas del centro.
- `172.29.1.0/24` $\rightarrow$ TIENE DHCP, donde los portátiles, móviles, y dispositivos no configurados en la red del centro se les asigna una dirección
   y pueden trabajar.

TODO: Rellenar con más información

# Red WiFi


## AULAMOVILWF 

Por ahora contamos con una serie de AP de Unifi configurados y gestionados desde Manager de Provalcom:

| Manager UNIFI | 
|---------------|
| 172.29.0.2|

| Unifi | Ubicación | SSID | Switch de la planta |
|-------|-----------|------|---------------------|
| 172.29.0.60 |           | AULAMOVILWF |                     |
| 172.29.0.61 |           | AULAMOVILWF |                     |
| 172.29.0.62 |           | AULAMOVILWF |                     |
| 172.29.0.63 |           | AULAMOVILWF |                     |
| 172.29.0.64 |           | AULAMOVILWF |                     |
| 172.29.0.65 |           | AULAMOVILWF |                     |
| 172.29.0.66 |           | AULAMOVILWF |                     |
| 172.29.0.67 |           | AULAMOVILWF |                     |
| 172.29.0.68 |           | AULAMOVILWF |                     |
| 172.29.0.69 |           | AULAMOVILWF |                     |


## WIFI_EDU

La Wifi_EDU permite a tod@s los miembros de la comunidad educativa iniciar sesión usando su **Identidad Digital** para 
autenticarse en el dominio.

Existen dos rangos de IPs (al menos), la de los profesores y la del alumnado. Se ha comprobado que están correctamente
dimensionadas en función del número de docentes y de alumnos que tenemos en el centro.

| Usuarios de la red | Dirección de la Red | Máscara | Número de clientes |
|--------------------|---------------------|---------|--------------------|
| profesores         | ??                  | 24      | 254                |
| alumnos            | ??                  | 22      | 1022               |

\newpage
# Aulas y Equipos

## Aulas Prefabricadas

Switch distribuidor en la AulaPrefabricada 2, la conexión va a la planta 2, al armario 
de comunicaciones del Aula de Informática 4.

| Aula | Hostname   | IP            | Versión | 
|------|------------|---------------|---------|
|AP1   | aulas-ap01 | 172.29.0.91   | lliurex | 
|AP2   | aulas-ap02 | 172.29.0.92   | lliurex | 
|AP3   | aulas-ap03 | 172.29.0.93   | lliurex | 
|AP4   | aulas-ap04 | 172.29.0.94   | lliurex | 
|AP5   | aulas-ap05 | 172.29.0.95   | lliurex | 
|AP6   | aulas-ap06 | 172.29.0.96   | lliurex | 
|AP7   | aulas-ap07 | 172.29.0.97   | lliurex | 
|AP8   | aulas-ap08 | 172.29.0.98   | lliurex |

## Taller de Tecnología

| Aula | Hostname | IP | Versión | Usuarios Volatiles | 
|------|----------|----|---------|--------------------|
|altillo| dynabook-tecno | 172.29.0.30| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.31| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.32| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.33| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.34| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.35| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.36| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.37| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.38| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.39| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.50| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.51| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.52| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.53| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.54| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.55| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.56| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.57| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.58| lliurex 21 | Estudiant| 
|altillo| dynabook-tecno | 172.29.0.59| lliurex 21 | Estudiant| 


## Gimnasio 

| Aula | Hostname | IP | Versión | senia-cdd |Usuarios Volatiles | 
|------|----------|----|---------|-----------|--------------------|
|Gimnasio  | gimnasio  | 172.29.0.40 | ??   | -         | No |


## Planta 0

| Aula          | Hostname | IP | Versión | 
|---------------|----------|----|---------|
|audiovisuales  | audiovisuales | ??  | lliurex    |
|biblioteca     |     ??        |  ??  | lliurex   |
|musica         | lliurex-aulamusica | 172.29.0.10   | focal   |
|aula0          |   ??       |    | ??   |
|salaguardias  |   salaguardias01       |  172.29.0.21  | noble   | 
|salaguardias  |   salaguardias02       |  172.29.0.22  | noble   |
|salaguardias  |   salaguardias03       |  172.29.0.23  | noble   |
|salaprofes    | salaprofes01         |  172.29.0.17  | noble   |
|salaprofes    | salaprofes02         |  172.29.0.18  | noble   | 
|salaprofes    | salaprofes03         |  172.29.0.19  | noble   | 
|salaprofes    | salaprofes04         |  172.29.0.20  | noble   | 
|conserjeria   | conserjeria         |  172.29.0.49  | ??   | 

### Carrito Portatiles P0

| Numero Ordenadores | Modelo | Sistemas Operativos | Actualizados| Wifi Configurada|
|------|-------------|------|------|-------|
| ??   |Toshiba Dynabook | LliureX/Windows | ??  | AULAMOVILWF |

## Planta 1

| Aula | Hostname |      IP        | Versión | 
|------|----------|----------------|---------|
|aula11  | aula11 | ??   | lliurex   | 
|aula12  | aula12 | ??   | lliurex   |  
|aula13  | aula13 | ??   | lliurex   | 
|aula14  | aula14 | ??   | lliurex   | 
|aula15  | aula15 | ??   | lliurex   |
|aula16  | aula16 | ??   | lliurex   | 
|aula17  | aula17 | ??   | lliurex   | 
|aula18  | aula18 | ??   | lliurex   |
|aula19  | aula19 | ??   | lliurex   |

### Laboratorios 

|     Aula     |    Hostname     |      IP      | Versión | 
|--------------|-----------------|--------------|---------|
| Lab.Ciencias  | lab-biologia   | 172.29.0.102 | lliurex  |
| Lab.FisicaQuimia | lab-fq      | 172.29.0.101 | noble    |


### Dep.Lenguas Extranjeras

Por actualizar

|     Aula     |    Hostname     |      IP      | Versión | 
|--------------|-----------------|--------------|---------|
| Lenguas Extranjeras| deplengex01 | 172.29.0.130 | ??   |
| Lenguas Extranjeras| deplengex02 | 172.29.0.132 | ??   |

## Planta 2

| Aula             | Hostname     | IP             | Versión |
|------------------|--------------|----------------|---------|
|dep. Humanidades  |dep-humanidades-01| 172.29.0.201   | ??   |
|dep. Humanidades  |dep-humanidades-02| 172.29.0.202   | ??   |
|dep. Ciencias  |dep-ciencias-01| 172.29.0.203   | ??   |
|aula21            |aula21        | 172.29.0.221   | ??   | 
|aula23            |aula23        | 172.29.0.223   | ??   |
|aula24            |aula24        | 172.29.0.224   | ??   |  
|aula25            |aula25        | 172.29.0.225   | ??   | 
|aula26            |aula26        | 172.29.0.226   | ??   | 
|aula27            |aula27        | 172.29.0.227   | ??   | 
|Plastica          |aulaplastica  | 172.29.0.231   | ??   |


### Carrito Portatiles P2

| Numero Ordenadores | Modelo | Sistemas Operativos | Actualizados| Wifi Configurada|
|------|-------------|------|------|-------|
| ??   |Toshiba Dynabook | LliureX/Windows | ??  | AULAMOVILWF |



\newpage
# Aulas Informatica

## Servidor Proxmox

| ProxMox| Hostname| IP | Version | 
|--------|-----------|----|---------|
| ??     |??         | ?? | ??|



## Aula Informatica 1

El servidor del Aula es el ordenador del profesor, tiene un DHCP (`dnsmasq)` y es capaz de enrutar. 

Pondré aquí la configuración del dnsmasq para tenerla más a mano.

Resumen:

| Red | Valor|
|-----|------|
| red |192.168.1.0/24 |
| gateway| 192.168.1.254 |
| ip server (red interna del aula) | 192.168.1.254 |
| ip server (red centro) | 172.29.0.251 |
| dhcp-range | 192.168.1.100-192.168.1.200 |

Los clientes tiene una reserva de IP basada en la MAC.


| Hostname | IP | Versión | senia-cdd | 
|----------|----|---------|-----------|
|servidor-aula01        | 192.168.1.254   | noble   | 
| aulainf01-pc11        | 192.168.1.11    |noble    | ??  | 
| aulainf01-pc12        | 192.168.1.12    |noble    | ??  | 
| aulainf01-pc13        | 192.168.1.13    |noble    | ??  | 
| aulainf01-pc14        | 192.168.1.14    |noble    | ??  |
| aulainf01-pc15        | 192.168.1.15    |noble    | ??  | 
| aulainf01-pc16        | 192.168.1.16    |noble    | ??  | 
| aulainf01-pc21        | 192.168.1.21    |noble    | ??  | 
| aulainf01-pc22        | 192.168.1.22    |noble    | ??  | 
| aulainf01-pc23        | 192.168.1.23    |noble    | ??  | 
| aulainf01-pc24        | 192.168.1.24    |noble    | ??  | 
| aulainf01-pc25        | 192.168.1.25    |noble    | ??  | 
| aulainf01-pc26        | 192.168.1.26    |noble    | ??  | 
| aulainf01-pc31        | 192.168.1.31    |noble    | ??  | 
| aulainf01-pc32        | 192.168.1.32    |noble    | ??  |
| aulainf01-pc33        | 192.168.1.33    |noble    | ??  | 
| aulainf01-pc34        | 192.168.1.34    |noble    | ??  |
| aulainf01-pc35        | 192.168.1.35    |noble    | ??  | 
| aulainf01-pc36        | 192.168.1.36    |noble    | ??  |

## Aula Informatica 2

El servidor del Aula es el ordenador del profesor, tiene un DHCP (`dnsmasq)` y es capaz de enrutar. 

Pondré aquí la configuración del dnsmasq para tenerla más a mano.

| Red | Valor|
|-----|------|
| red |192.168.2.0/24|
| gateway| 192.168.2.254|
| ip server (red interna del aula) | 192.168.2.254|
| ip server (red del centro) | 172.29.0.252 |
| dhcp-range | 192.168.2.100-192.168.2.200 |


| Hostname | IP | Versión | senia-cdd |
|----------|----|---------|-----------|
| aulainf02-server         | 192.168.2.254   | noble   | ??|
| aulainf02-pc11        | 192.168.2.11    |noble    |  ??          | 
| aulainf02-pc12        | 192.168.2.12    |noble    |  ??          |
| aulainf02-pc13        | 192.168.2.13    |noble    |  ??          |
| aulainf02-pc14        | 192.168.2.14    |noble    |  ??          |
| aulainf02-pc15        | 192.168.2.15    |noble    |  ??          |
| aulainf02-pc16        | 192.168.2.16    |noble    |  ??          |
| aulainf02-pc21        | 192.168.2.21    |noble    |  ??          |
| aulainf02-pc22        | 192.168.2.22    |noble    |  ??          |
| aulainf02-pc23        | 192.168.2.23    |noble    |  ??          |
| aulainf02-pc24        | 192.168.2.24    |noble    |  ??          |
| aulainf02-pc25        | 192.168.2.25    |noble    |  ??          |
| aulainf02-pc26        | 192.168.2.26    |noble    |  ??          |
| aulainf02-pc31        | 192.168.2.31    |noble    |  ??          |
| aulainf02-pc32        | 192.168.2.32    |noble    |  ??          |
| aulainf02-pc33        | 192.168.2.33    |noble    |  ??          |
| aulainf02-pc34        | 192.168.2.34    |noble    |  ??          |


## Aulas 3, 4 y 5

El servidor del Aula es el ordenador que se encuentra en el Aula de Informática 4 encima de los Switches, tiene un DHCP (`dnsmasq)` y es capaz de enrutar. 

Pondré aquí la configuración del dnsmasq para tenerla más a mano. 

Las tres aulas pertenecen a la misma red (**/23**).

Recordad que existe la posibilidad de *prohibir* determinados dominios usando en `dnsmasq`. Como ya se comentó el año pasado.


### Servidor Aula 4

*Provisional*:

Establecer aqui el script dables

```shell
# IPTABLES Script del NAT de las Aulas de Ciclos
```

| IP del Centro   | Red de las Aulas de Informatica |
|-----------------|---------------------------------|
| 172.29.0.254/23 | 192.168.4.254/23 |


| Red | Valor|
|-----|------|
| red |192.168.4.0/23|
| gateway| 192.168.4.254|
| ip server (red de aulas 3,4 y 5)| 192.168.4.254 |
| ip server (red de centro| 172.29.0.254 |
| dhcp-range | 192.168.5.100-192.168.5.200|



### Aula Informatica 3


| Hostname | IP | Versión | senia-cdd | 
|----------|----|---------|-----------|
|aulainf03-pcprofe | 192.168.4.99  | noble     | ?? |      
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |       
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |
|aulainf03-pc      |                |noble    | ??  |

### Aula Informática 4

| Hostname | IP | Versión | senia-cdd |
|----------|----|---------|-----------|
| aulainf04-pcprofe     | 192.168.4.199    |noble    | ?? |              
| aulainf04-pc01        | 192.168.4.101    |noble    | ?? |              
| aulainf04-pc02        | 192.168.4.102    |noble    | ?? | 
| aulainf04-pc03        | 192.168.4.103    |noble    | ?? | 
| aulainf04-pc04        | 192.168.4.104    |noble    | ?? | 
| aulainf04-pc05        | 192.168.4.105    |noble    | ?? | 
| aulainf04-pc06        | 192.168.4.106    |noble    | ?? | 
| aulainf04-pc07        | 192.168.4.107    |noble    | ?? | 
| aulainf04-pc08        | 192.168.4.108    |noble    | ?? | 
| aulainf04-pc09        | 192.168.4.109    |noble    | ?? | 
| aulainf04-pc10        | 192.168.4.110    |noble    | ?? | 
| aulainf04-pc11        | 192.168.4.111    |noble    | ?? | 
| aulainf04-pc12        | 192.168.4.112    |noble    | ?? | 
| aulainf04-pc13        | 192.168.4.113    |noble    | ?? | 
| aulainf04-pc14        | 192.168.4.114    |noble    | ?? | 
| aulainf04-pc15        | 192.168.4.115    |noble    | ?? | 
| aulainf04-pc16        | 192.168.4.116    |noble    | ?? | 
| aulainf04-pc17        | 192.168.4.117    |noble    | ?? | 
| aulainf04-pc18        | 192.168.4.118    |noble    | ?? | 
| aulainf04-pc19        | 192.168.4.119    |noble    | ?? | 
| aulainf04-pc20        | 192.168.4.120    |noble    | ?? | 
| aulainf04-pc21        | 192.168.4.121    |noble    | ?? | 

### Aula Informática 5

| Hostname | IP | Versión | senia-cdd |
|----------|----|---------|-----------|
|aulainf05-pcprofe        | 192.168.5.100   | noble  | ?? |
| aulainf05-pc01          | 192.168.5.1     |noble   | ?? |
| aulainf05-pc02          | 192.168.5.2     |noble   | ?? |
| aulainf05-pc03          | 192.168.5.3     |noble   | ?? |
| aulainf05-pc04        | 192.168.5.4     |noble   | ?? |
| aulainf05-pc05        | 192.168.5.5     |noble   | ?? |
| aulainf05-pc06        | 192.168.5.6     |noble   | ?? |
| aulainf05-pc07        | 192.168.5.7     |noble   | ?? |
| aulainf05-pc08        | 192.168.5.8     |noble   | ?? |
| aulainf05-pc09        | 192.168.5.9     |noble   | ?? |
| aulainf05-pc10        | 192.168.5.10    |noble   | ?? |
| aulainf05-pc11        | 192.168.5.11    |noble   | ?? |
| aulainf05-pc12        | 192.168.5.12    |noble   | ?? |
| aulainf05-pc13        | 192.168.5.13    |noble   | ?? |
| aulainf05-pc14        | 192.168.5.14    |noble   | ?? |
| aulainf05-pc15        | 192.168.5.15    |noble   | ?? |
| aulainf05-pc16        | 192.168.5.16    |noble   | ?? |
| aulainf05-pc17        | 192.168.5.17    |noble   | ?? |
| aulainf05-pc18        | 192.168.5.18    |noble   | ?? |
| aulainf05-pc19        | 192.168.5.19    |noble   | ?? |
| aulainf05-pc20        | 192.168.5.20    |noble   | ?? |
| aulainf05-pc21        | 192.168.5.21    |noble   | ?? |
| aulainf05-pc22        | 192.168.5.22    |noble   | ?? |
| aulainf05-pc23        | 192.168.5.23    |noble   | ?? |
| aulainf05-pc24        | 192.168.5.24    |noble   | ?? |


