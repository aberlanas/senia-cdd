# Receta para Dynabooks

Estado y recetas para Dynabooks

## LliureX

El LliureX está instalado en versión Bionic (16.04). Se está manteniendo actualizado usando:

```bash
sudo lliurex-upgrade -u
```

### Guia para No Ser Una Loca de Los Gatos

```bash
sudo add-apt-repository ppa:ticsenia/ppa
sudo apt update 

# instalacion del paquete del Virus T
sudo apt install senia-lliurex-mutagen
sudo senia-lliurex-mutation
```

## Aula de Tecnología

Usando el `zero-center` se ha instalado todo el Software relacionado 
con tecnología (arduino, visualino, scratch,...).

Receta para Tecnologia (Junio 2024):

```bash
# Actualizamos con LliureX Upgrade
sudo lliurex-upgrade -u

# Anyadimos el repos de la Senia
sudo apt-add-repository ppa:ticsenia/ppa

# Instalamos el mutageno 
sudo apt install senia-lliurex-mutagen

# Ejecutamos el mutageno
sudo senia-lliurex-mutation

# Congelamos al usuario Estudiant
sudo apt install senia-frozen-tecno-users

# Creamos al usuario tecno
sudo adduser tecno

```

Ese paquete: `senia-frozen-tecno-users`, lo que hace es que crea el mecanismo para borrar los contenidos
del `$HOME` del usuario `Estudiant` y del usuario `invitado`. En realidad el usuario `invitado` ya se borra 
con el *setup* básico del `senia-frozen-users`. Lo que se hace es que se hace uso del sistema de plugins
que tiene el script.

## IPs Fijas

Se podrían indicar aquí las IPs de los diferentes equipos del Altillo de Tecnología.


