# Receta para Dynabooks

Estado y recetas para Dynabooks

## LliureX

Existen dos versiones de este LliureX, la oficial (Bionic) y algunas pruebas que se hicieron y se migraron
los LliureX 19 a LliureX 21 (focal). Se mantienen las mismas instrucciones ya que por suerte todo funciona
de manera parecida en bionic y en focal.

El LliureX está instalado en versión Bionic (16.04). Se está manteniendo actualizado usando:

```bash
sudo lliurex-upgrade -u
```

### Guia para No Ser Una Loca de Los Gatos (PS).

```bash
sudo add-apt-repository ppa:ticsenia/ppa
sudo apt update 

# instalacion del paquete del Virus T
sudo apt install senia-lliurex-mutation
sudo senia-lliurex-mutation
```



### Senia Kiosko

Desde la versión del 7 de Mayo del 2024, el paquete del `senia-kiosko` configura el autologin y 
abre el firefox por defecto en la URL que se le indica en el paquete. 

Para instalarlo basta con ejecutar:

```bash
sudo apt install senia-kiosko
```




## Aula de Tecnología

Usando el `zero-center` se ha instalado todo el Software relacionado 
con tecnología (arduino, visualino, scratch,...).

IPs fijas.
