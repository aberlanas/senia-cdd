# Guia para pasar de Xubuntu a Senia

Esta guía esta pensada para ser actualizada en futuras versiones.

Por ahora se mantiene para la instalación y actualización de **Xubuntu 24.04 (Noble Numbat)**.

## Instalación de Xubuntu

Se instala Xubuntu actualizando el propio actualizador, pero sin descargar el soporte de multimedia
ni los drivers (el motivo son los repositorios que vienen por defecto que se encuentra *proxyficados* en
el IES).

Es conveniente instalar con el usuario *tic* y el nombre del *PC* con el siguiente formato:

* aulainf0X-pcXX

Una vez instalado se reinicia.

## Configuración de los Orígenes del Software 

El fichero `/etc/apt/sources.list` sigue funcionando, pero se recomienda usar el *directorio 
de configuración* situado en `/etc/apt/sources.list.d/`.

El fichero `/etc/apt/sources.list.d/ubuntu.sources` ha de quedarse así:

```shell
## See the sources.list(5) manual page for further settings.
Types: deb
URIs: http://ubuntu.cica.es/ubuntu
Suites: noble noble-updates noble-backports
Components: main universe restricted multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg

## Ubuntu security updates. Aside from URIs and Suites,
## this should mirror your choices in the previous section.
Types: deb
URIs: http://ubuntu.cica.es/ubuntu
Suites: noble-security
Components: main universe restricted multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg
```

Se puede editar con `nano` por ejemplo. O usando `sed`.

Una vez realizada esta operación, ya tenemos los repositorios que queremos para 
las actualizaciones e instalaciones de software, así que:


```bash
sudo apt update && sudo apt full-upgrade --yes
```

## Repositorios de La Senia

```bash
# Anyadimos el repo de la Senia
sudo add-apt-repository ppa:ticsenia/ppa

# Instalamos el metapaquete que sirve de arrastrar
# dependencias, scripts y de todo.
sudo apt install senia-cdd-xfce
```

A las preguntas que nos hace:

* `postfix`: Sitio de Internet.
* `msttcorefonts`: Aceptamos la EULA.
* `libdvd-css`: Si y p'alante.


Reiniciamos y veremos que los usuarios han sido creados y todo está como toca. 



