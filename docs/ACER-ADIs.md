# Documentación ACER - ADIS 2023

Para los diferentes ACER que se han suministrado con las ADIs, se ha preparado un paquete 
que configura algunos parámetros.

Version : Lliurex 21 /Windows 11

## Actualización para Torpes : Junio 2024

- Entrar en LliureX en el Grub. 
- Comprobar que por defecto sale el login del sddm que valida directamente al Invitado.
- Seleccionar otro login (Defecto) y entrar con tic.
- Abrir una terminal y ejecutar las siguientes ordenes:

```bash
sudo lliurex-upgrade -u
sudo add-apt-repository ppa:ticsenia/ppa
sudo apt install senia-cdd-lliurex
sudo reboot
```

Asegurarse que en el reinicio la ventana del sddm ya es la que permite directamente iniciar sesión
con el usuario "invitado" de LliureX en vez de que tengan que validarse con el usuario del AD. En caso
de que no sea así, por favor buscadme averiguaremos qué es lo que pasa, ya que se trata de un cambio 
que hay que hacer en un fichero *.qml* que es el que define las entradas.

Esto hay que hacerlo en todos los Acer de las ADIs, así prepararé una excel y os vais apuntando los hechos.

## LliureX 21

```bash
sudo add-apt-repository ppa:ticsenia/ppa

# instalacion del paquete de invitado
sudo apt install senia-cdd-lliurex

```
### senia-cdd-lliurex

Arrastra todas las dependencias y configuraciones preparadas.

## Windows 11

Pues la patata esa de Windows con AD. ^_^. 


