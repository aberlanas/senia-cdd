#!/usr/bin/python3.11

#Python code to illustrate parsing of XML files
# importing the required modules
import sys
import csv
import requests
import xml.etree.ElementTree as ET
from rich import print



# Valores por defecto
mVerbose = False;

def parseXML(xmlfile):
  
    # create element tree object
    tree = ET.parse(xmlfile)
  
    # get root element
    root = tree.getroot()
  
    # create empty list for news items
    newsitems = []
  
  
    # iterate news items
    for item in root.findall('./horarios_grupo'):
  
        # empty news dictionary
        news = {}
  
        # iterate child elements of item
        print(" *** Docentes *** ")
        for child in item:
            docente_aux = child.attrib['docente']
            esta = False
            for doc in root.findall('./docentes'):
               for child2 in doc:
                   if docente_aux == child2.attrib['documento']:
                       esta = True
               if esta:
                    if mVerbose:
                        print(" Esta controlado "+ docente_aux)
                    else:
                        pass
               else:
                   print(" - [ Docente ] : Un docente salvaje: "+docente_aux)
                  
        print(" *** Aulas *** ")
        for child in item:
            aula_aux = child.attrib['aula']
            esta = False
            for doc in root.findall('./aulas'):
               for child2 in doc:
                   if aula_aux == child2.attrib['codigo']:
                       esta = True
               if esta:
                   if mVerbose:
                       print(" Aula : esta controlado "+ aula_aux)
                   else:
                       pass
               else:
                   print(" - [ Aulas ] : Un Aula salvaje: :backhand_index_pointing_right: "+aula_aux+" - Dia : "+str(child.attrib['dia_semana'])+" Grupo: "+str(child.attrib['grupo']) +" en la sesion: "+str(child.attrib['sesion_orden']) )    


if __name__ == "__main__":
  
    # calling main function
    if len(sys.argv) != 3:
        print(" * [USAGE] : ")
        print("./comprobador.py [modo] ficheroHorarios.xml")
        print(" * modos : ")
        print("   -v|--verbose : Muestra informacion extendida")
        print("   -q|--quiet   : Solo muestra errores")
        print()
        sys.exit(1)
        
    if sys.argv[1] == "--verbose" or sys.argv[1] == "-v":
        mVerbose = True
        
    fichHorarios=sys.argv[2]
    print(" * Trabajando con "+str(fichHorarios))
    parseXML(fichHorarios)
