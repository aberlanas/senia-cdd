#!/bin/bash
#

SENIA_BASE="/home/aberlanas/GitLab/senia-cdd/"

find $SENIA_BASE -maxdepth 1 -mindepth 1 -xtype d -name "*.install"| while read line; do

	package_name=$(basename $line| cut -d . -f1)

	
	echo " WiP : $package_name"

	rc=0

	cat $SENIA_BASE/debian/control | grep -q "Package: $package_name" || rc=1

	if [ $rc -eq 0 ]; then
		echo " -- Presente : $package_name "
	else
		echo -e " -- Liberar :\n git rm -r $package_name.install"
		git rm -r $package_name.install
	fi

	echo " "
	echo " "
done



exit 0
