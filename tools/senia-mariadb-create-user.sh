#!/bin/bash
#

# This script is licensed under GPL v3

if [ $(id -u) -ne 0 ]; then
	echo " You Shall not pass!!!"
	exit 1
fi

echo " Estableciendo el usuario senia y password seniadb "

cat <<EOF> /tmp/senia-create-user.sql 

CREATE USER IF NOT EXISTS 'senia'@'localhost' IDENTIFIED BY 'seniadb';
GRANT ALL PRIVILEGES ON *.* TO 'senia'@localhost IDENTIFIED BY 'seniadb';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'senia'@localhost;

EOF

mysql -u root < /tmp/senia-create-user.sql

exit 0
