#!/bin/bash
#

# Algunas variables
AUXFILE=$(mktemp /tmp/fileDOGV.XXXX)
dentro=0
MODULO_CURRENT="/dev/null"
CSVS="./csvs/"
CICLO=$1

mkdir -p ./csvs/$CICLO/
rm -f ./csvs/$CICLO/*.csv

shift

cat $1| while read line; do
	
	rc=0
	echo $line | grep -q "ANEXO" || rc=1

	if [ $rc = 0 ]; then
		dentro=1
	fi

	rf=0
	echo $line | grep -q "ANNEX II" || rf=1
	
	if [ $rf -eq 0 ]; then
		echo " Fin del anexo"
		exit 0
	fi

	if [ $dentro -eq 1 ]; then
		rm=0
		echo $line | grep "^Módulo" || rm=1

		if [ $rm -eq 0 ]; then
			echo -n " * Trabajando con el nuevo modulo : "
			NOMBREMODULO=$(echo $line| cut -d ":" -f 2)
			MODULO_CURRENT=$CSVS/$CICLO/$(echo $NOMBREMODULO | sed -e "s% %-%g;s%á%a%g;s%é%e%g;s%í%i%g;s%ó%o%g;s%ú%u%g").csv
			echo $MODULO_CURRENT
		fi

		var=$(echo $line| wc -c)

		if [ $var -ne 0 ]; then
			echo $line >> $MODULO_CURRENT
		fi
		
	fi
done

for f in $(find $CSVS/$CICLO/ -xtype f -name "*.csv" ); do 

	cat $f | grep -v "^$" > $f.bak
	sed -i "/^Módulo/d" $f.bak
	sed -i "/^Código/d" $f.bak
	sed -i "/^Duración/d" $f.bak
	sed -i "/^Contenidos:/d" $f.bak

	mv $f.bak $f

done

exit 0
