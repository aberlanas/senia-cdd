#!/usr/bin/python3

# This Script is licensed under GPL v3 
# or higher.

from openpyxl import Workbook
from openpyxl.styles import PatternFill, Alignment, Font
from openpyxl.styles.colors import Color

import csv
import os
import glob
import itertools
import sys

result = glob.glob( 'csvs/'+sys.argv[1]+'/**.csv' )

wb = Workbook()

c = Color("BFBFBF")
cContenido = Color("DFDFDF")

for csvfile in result:
    with open(csvfile, "r") as my_file:
        basename = os.path.basename(csvfile).split(".")[0]
        if len(basename) > 30:
            basename=basename[:10]
        wb.create_sheet(basename)
        ws = wb[basename]
        # pass the file object to reader()
        file_reader = csv.reader(my_file)
        #do this for all the rows
        fila = 2
        ws.append(["Contenidos del Curriculo","Tecnologias 2023", "Autopropuesta de Mejora ", "Propuestas Externas"])
        ws.cell(column=1,row=1).fill = PatternFill(fgColor=c, fill_type = "solid")
        ws.cell(column=1,row=1).font = Font(color="000000",bold=True)
        ws.cell(column=1,row=1).alignment = Alignment(horizontal="center",vertical="center")
        ws.cell(column=2,row=1).fill = PatternFill(fgColor=c, fill_type = "solid")
        ws.cell(column=2,row=1).font = Font(color="000000",bold=True)
        ws.cell(column=2,row=1).alignment = Alignment(horizontal="center",vertical="center")
        ws.cell(column=3,row=1).fill = PatternFill(fgColor=c, fill_type = "solid")
        ws.cell(column=3,row=1).font = Font(color="000000",bold=True)
        ws.cell(column=3,row=1).alignment = Alignment(horizontal="center",vertical="center")
        ws.cell(column=4,row=1).fill = PatternFill(fgColor=c, fill_type = "solid")
        ws.cell(column=4,row=1).font = Font(color="000000",bold=True)
        ws.cell(column=4,row=1).alignment = Alignment(horizontal="center",vertical="center")
        column_width = 10
        for i in file_reader:
            # print the row
            #print(i[0])
            
            auxWidth = len(str(i[0]))
            
            if column_width < auxWidth:
                column_width = auxWidth
            
            if len(i) > 1:
                aux = "".join(i)
                i = [aux]
            
            ws.append(i)
            if not str(i[0]).startswith("-") :
                print("Fila : "+str(fila)+" ->  "+i[0])
                ws.cell(column=1,row=fila).fill = PatternFill(fgColor=cContenido, fill_type = "solid")
                ws.cell(column=2,row=fila).fill = PatternFill(fgColor=cContenido, fill_type = "solid")
                ws.cell(column=3,row=fila).fill = PatternFill(fgColor=cContenido, fill_type = "solid")
                ws.cell(column=4,row=fila).fill = PatternFill(fgColor=cContenido, fill_type = "solid")
                ws.cell(column=1,row=fila).font = Font(color="000000",bold=True)
                ws.cell(column=1,row=fila).alignment = Alignment(horizontal="left",vertical="center")
                ws.row_dimensions[fila].height=25
            
            
            fila = fila + 1
        
        ws.column_dimensions["A"].width=column_width
        ws.row_dimensions[1].height=45
        ws.column_dimensions["B"].width=25
        ws.column_dimensions["C"].width=25
        ws.column_dimensions["D"].width=25

# Save the file
wb.remove(wb["Sheet"])
wb.save(sys.argv[1]+".xlsx")
