# Senia CDD 2025

Documentación de los diferentes paquetes y necesidades de Software de las aulas del centro
IES La Senia de Paiporta, aunque podría adaptarse a cualquier centro educativo. 

Actualmente contamos con configuración y paquetería que aplica a las diferentes aulas e infraestructura
del centro.

## Introducción

La idea general es crear un paquete (`senia-cdd-desktop`), que al instalarse prepare 
el entorno y arrastre todo lo necesario para preparar los ordenadores de las aulas de 
informática ( y los personales ) con todo lo necesario para ser utilizados dentro de 
la Infraestructura del IES.

A continuación se describen las acciones que se realizan, con el objetivo de presentar
un plano general de lo que ocurre por debajo, siempre se pueden examinar los diferentes
paquetes y comprobar el código fuente ^_^.

## Instalación

Para instalar sobre Ubuntu 24.04 y Ubuntu 24.10

```
sudo add-apt-repository ppa:ticsenia/ppa
sudo apt install senia-cdd-desktop
```

## Dependencias generales

Aquí se muestra un pequeño gráfico de las dependencias 
iniciales.

![senia-cdd-desktop](./docs/senia-cdd-desktop.png)

### senia-dependences

Estas son las dependencias de primer nivel de `senia-dependences`.

![senia-dependences](./docs/senia-dependences.png)

### senia-backgrounds

Instala el fondo de pantalla del para ser usado tanto en el `lightdm` (gestor de entrada) como con el `pcmanfm` (Escritorio del usuario).

### senia-browser-settings

Usando el mecanismo proporcionado por Mozilla, instala un fichero de `policies.json` que establece mecanismos interesantes en el navegador.

### senia-cdd

Aunque su principal función es la de establecer la red fundamental de dependencias
y asegurarse de ser el "*hilo conductor*" de los paquetes en la infraestructura, este paquete instala el binario : `senia-version` que muestra al ejecutarse los **componentes** y la versión del propio `senia-cdd`.

### senia-cdd-lliurex

Un paquete destinado a ser instalado sobre LliureX, pero por ahora queda un poco en espera, ya que no sabemos bien si nos permitirán hacerlo.

### senia-cdd-lxqt

Paquete que instala las dependencias y la configuración para usar una sesión de Lubuntu, pero con un par de modificaciones para adecuarse a la configuración del IES.

### senia-cdd-xfce

Paquete de la infraestructura sobre Xfce4, poco a poco debería ir migrándose hacia lxqt.

### senia-cron

Paquete que instala un fichero de cron para apagar todos los ordenadores del IES a las 23:00 cada noche.

### senia-firstboot

Uno de los paquetes que acumulan más utilidades y herramientas con el objetivo de hacer
más fácil la vida de los administradores de sistemas del IES.

### senia-frozen-users

Un paquete que al instalarse, limpia los `HOME` de los usuarios seleccionados.

### senia-kiosko

Una manera *sencilla* de establecer un usuario en autologin y que directamente se abra
el firefox a pantalla completa con una página determinada.

### senia-lliurex-configs

**TODO**

## Usuarios 

Se crean una serie de usuarios para ser utilizados en las aulas 
de informática, y se les añade al grupo `docker` y se les permite
capturar paquetes de la red, típicamente mediante `wireshark`. 

## Cosas por hacer

* Quitar todos los paquetes que sobran y dejar esto minimalista.

## Instalacion Sobre Xubuntu Noble

* [De Xubuntu a la Senia](docs/Xubuntu-To-Senia.md)

## Enlaces de Interés

    Documentación de la actualización.
* [Infraestructura del IES](docs/Infraestructura.md)

    La Infraestructura del IES (a fecha de Febrero de 2024) a falta de que nos cambien cosas en los Switches.
    pero toca estar preparados para todo. 

* Documentación más bien vieja, pero útil
    
    * [Migración a Jammy para PSs](docs/MigracionAJammy.md)
    * [PMB XarxaLlibres](docs/PMB.md)

